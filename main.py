import os
import shutil
import hashlib
import argparse
import time
import logging

# Set up logging
logging.basicConfig(filename='sync_log.txt', level=logging.INFO, format='%(asctime)s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')


def calculate_md5(file_path):
    """Calculate the MD5 hash of a file."""
    md5_hash = hashlib.md5()
    with open(file_path, "rb") as file:
        while True:
            data = file.read(8192)
            if not data:
                break
            md5_hash.update(data)
    return md5_hash.hexdigest()


def sync_folders(source_folder, replica_folder):
    """Synchronize the source folder with the replica folder."""
    for root, _, files in os.walk(source_folder):
        for file in files:
            source_file_path = os.path.join(root, file)
            replica_file_path = os.path.join(replica_folder, os.path.relpath(source_file_path, source_folder))

            if not os.path.exists(replica_file_path) or calculate_md5(source_file_path) != calculate_md5(
                    replica_file_path):
                logging.info(f"Copying {source_file_path} to {replica_file_path}")
                shutil.copy2(source_file_path, replica_file_path)

    # Remove files in the replica folder that do not exist in the source folder
    for root, _, files in os.walk(replica_folder):
        for file in files:
            replica_file_path = os.path.join(root, file)
            source_file_path = os.path.join(source_folder, os.path.relpath(replica_file_path, replica_folder))

            if not os.path.exists(source_file_path):
                logging.info(f"Removing {replica_file_path}")
                os.remove(replica_file_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Folder synchronization tool")
    parser.add_argument("source_folder", help="Source folder path")
    parser.add_argument("replica_folder", help="Replica folder path")
    parser.add_argument("sync_interval", type=int, help="Synchronization interval in seconds")
    args = parser.parse_args()

    while True:
        sync_folders(args.source_folder, args.replica_folder)
        print("Synchronization completed.")
        time.sleep(args.sync_interval)
