# syncProject



## Introduction

This program synchronizes the source folder with the replica folder by periodically checking
for changes. It uses MD5 hashing to compare file contents and copies or removes files as necessary.
The sync_log.txt file will contain logs of file operations, and you can adjust the synchronization interval as needed.

## How to run:

To run the program, you can use the command line like this:
```
python main.py /source/folder/path /replica/folder/path 60
```
